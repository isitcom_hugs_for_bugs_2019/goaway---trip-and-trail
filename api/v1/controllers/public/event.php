<?php


App::get('find', function () {

    $max = App::$request["max"] ?? 10;
    $cp = App::$request["cp"] ?? 0;
    $q = App::$request["q"] ?? "";
    $q = str_replace("+", " ", $q);
    $sortby = App::$request["sortby"] ?? "price";
    $sort_type = App::$request["sort_type"] ?? "desc";
    $min_price = App::$request["min_price"] ?? 0;
    $max_price = App::$request["max_price"] ?? 10000000000;

    $req  =  App::db()->select('*')
        ->from("event")
        ->where(
            " (LOWER(title) LIKE LOWER(concat('%',?, '%'))
         or LOWER(description) LIKE LOWER(concat('%',?, '%')) ) and price>=? and price < ?  ",
            [$q, $q, $min_price, $max_price]
        )
        ->orderBy($sortby . "  " . $sort_type);

    $total_rows = $req->execute()->rowCount();
    $total_pages  =  ceil($total_rows / $max);
    $from =  ceil(($cp) * $max);

    $items = $req->limit("$from,$max")->execute();

    App::$response["detail"]  = [
        "q" => $q,
        "count" => $items->rowCount(),
        "total" => $total_rows,
        "pages" => $total_pages
    ];
    App::$response["items"] = $items->fetchAll();
});
