<?php

App::get('all', function () {
    $max = App::$request["max"] ?? 10;
    App::$response["items"] = App::db()
        ->select('e.id,e.title,e.city')
        ->from("event e")
        ->join("left join category c on c.id = e.category_id ")
        ->orderBy("c.id desc")
        ->where("user_id=?", TOKEN_ID)
        ->limit($max)
        ->execute()->fetchAll();
});

App::get('', function () {
    $id = App::$request["id"];
    App::$response["info"] = App::db()
        ->select('*')
        ->from("event ")
        ->where("user_id=? and id=?", [TOKEN_ID, $id])
        ->execute()->fetchObject();
});


App::post('', function () {
    $duration = @App::$request["duration"];
    $price = @App::$request["price"];
    $title = @App::$request["title"];
    $description = @App::$request["description"];
    $image = @App::$request["image"] ?? "null";
    $city = @App::$request["city"];
    $date = @App::$request["date"];
    $category_id = @App::$request["category_id"];

    Func::emptyCheck([$title, $duration, $price, $city, $date, $category_id]);

    $image = App::upload($image, "images/event/", Func::randomString(10));

    $data = [
        "user_id" => TOKEN_ID,
        "duration" => $duration,
        "price" => $price,
        "title" => $title,
        "description" => $description,
        "image" => $image,
        "city" => $city,
        "date" => $date,
        "category_id" => $category_id
    ];
    App::db()->insert("event", $data, "user_id=?", [TOKEN_ID]);
});


App::put('', function () {
    $id = @App::$request["id"];
    $duration = @App::$request["duration"];
    $price = @App::$request["price"];
    $title = @App::$request["title"];
    $description = @App::$request["description"];
    $image = @App::$request["image"] ?? "null";
    $city = @App::$request["city"];
    $date = @App::$request["date"];
    $category_id = @App::$request["category_id"];

    Func::emptyCheck([$title, $duration, $price, $city, $date, $category_id]);


    $data = [
        "duration" => $duration,
        "price" => $price,
        "title" => $title,
        "description" => $description,
        "image" => $image,
        "city" => $city,
        "date" => $date,
        "category_id" => $category_id
    ];
    App::db()->update("event", $data, "user_id=? and id=?", [TOKEN_ID, $id]);
});


App::delete('', function () {
    $id = @App::$request["id"];
    Func::emptyCheck([$id]);
    App::db()->delete("event", "user_id=? and id=?", [TOKEN_ID, $id]);
});
