<?php

class Func
{

    public static function time()
    {
        return strtotime('now Europe/Paris');
    }

    public static function setCookie($cookie_name, $cookie_value)
    {
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
    }

    public static function assets($path)
    {
        return BASE_URL . $path;
    }

   

    public static function shared($fileName)
    {
        include_once "controllers/shared/" . $fileName . ".php";
    }

    public static function path($url = null)
    {
        return BASE_URL . $url;
    }

    public static function pathApi($url = null)
    {
        return API_URL . $url;
    }

    public static function getCookie($cookie_name)
    {
        return @$_COOKIE[$cookie_name];
    }

    public static function spaceFix($value)
    {
        return trim(preg_replace('/\s+/', ' ', $value));
    }

    public static function urlEexists($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    public static function request($url, $method = "GET", $data = [])
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $method = strtoupper($method);
        switch ($method) {
            case "GET":
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
                break;
            case "POST":
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
        }

        $response = curl_exec($curl);
        print_r($response);
        $data = json_decode($response, false);
        /* Check for 404 (file not found). */
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // Check the HTTP Status code
        switch ($httpCode) {
            case 200:
                $error_status = "200: Success";
                return ($data);
                break;
            case 404:
                $error_status = "404: API Not found";
                break;
            case 500:
                $error_status = "500: servers replied with an error.";
                break;
            case 502:
                $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
                break;
            case 503:
                $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
                break;
            default:
                $error_status = "Undocumented error: " . $httpCode . " : " . curl_error($curl);
                break;
        }
        curl_close($curl);
        return $error_status;
        die;
    }

    public static function checkImage($image)
    {
        if (strpos($image, "http") !== false) {
            return $image;
        } else {
            return BASE_URL . "$image";
        }
    }

    public static function setArray($array, $propertie, $value, $where = 'left')
    {
        foreach ($array as $key => $val) {
            $prop_value = $array[$key][$propertie];
            if (!empty($prop_value)) {
                if ($where == 'left') {
                    $array[$key][$propertie] = self::checkImage($value . $prop_value);
                } else {
                    $array[$key][$propertie] = self::checkImage($prop_value . $value);
                }
            }
        }

        return $array;
    }

    //Remove the specific properties of an object in an array (Ex password or email)
    function unset($array, $properties)
    {
        $properties = explode(',', $properties);
        foreach ($array as $key => $val) {
            foreach ($properties as $propertie) {
                unset($array[$key][$propertie]);
            }
        }

        return $array;
    }

    //To make slug (URL string)
    public static function slugify($text, $maxLenght = 5)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return substr($text, 0, $maxLenght);
    }

    public static function randomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function urlMix($name, $nameLength = 10, $randomLength = 10)
    {
        return trim(self::slugify($name, $nameLength) . '-' . self::randomString($randomLength), '-');
    }
}
