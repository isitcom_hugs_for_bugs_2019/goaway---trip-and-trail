<?php
require_once './classes/Router.php';

class App extends Router
{
    public static $request = [];

    public static function init()
    {
        self::$request = self::initRequest();
    }


    public static function initRequest()
    {
        if (!$_post   = json_decode(file_get_contents("php://input"))) {
            $_post    = json_decode(json_encode($_POST), false);
        }
        $_get         = json_decode(json_encode($_GET), FALSE);
        return   array_merge((array) $_get, (array) $_post);
    }

    public static function request($param = null)
    {
        if ($param != null) {
            return self::$request[$param];
        } else {
            return self::$request;
        }
    }

}