<html>

<head>
    <title><?= APP_NAME ?></title>
    <?php Func::shared("head") ?>
</head>

<body>
    <div id="app">
        <?php Func::shared("header") ?>
        <?php include "components/nav.php" ?>



        <?php include "components/footer.php" ?>
    </div>

    <?php Func::shared("script") ?>

</body>
</html>