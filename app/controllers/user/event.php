<html>

<head>
    <title><?= APP_NAME ?></title>
    <?php Func::shared("head") ?>
</head>

<body>
    <div id="app">
        <?php Func::shared("header") ?>
        <?php include "components/nav.php" ?>

        <div class="dashboard-content">
            <div class="add-listing-headline">
                <h3><i class="sl sl-icon-doc"></i>My Trips</h3>
            </div>


            <div class="dashboard-list-box margin-top-0">
                <ul>


                    <li v-for="trip in trips">
                        <div class="list-box-listing">
                            <div class="list-box-listing-content">
                                <div class="inner">
                                    <h3><a href="#">{{trip.title}}</a></h3>
                                    <span>City : {{trip.city}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="buttons-to-right">
                            <a class="button gray" :href="api_base+''+trip.id"><i class="sl sl-icon-note"></i> Edit</a>
                            <a @click="deleteItem(trip.id)" class="button gray"><i class="sl sl-icon-close"></i> Delete</a>
                        </div>
                    </li>



                </ul>
            </div>

        </div>

        <?php include "components/footer.php" ?>
    </div>

    <?php Func::shared("script") ?>

    <script>
        axios.defaults.headers.common['X-Token'] = getCookie("X-Token");

        var app = new Vue({
            el: '#app',
            data: {
                api_base: "<?= Func::path("user/update-event/") ?>",
                trips: []
            },
            methods: {
                deleteItem(id) {
                    axios.delete("<?= Func::pathApi("user/event/") ?>?id=" + id)
                        .then((response) => {
                            this.getAll();
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                },
                getAll() {
                    axios.get("<?= Func::pathApi("user/event/all") ?>")
                        .then((response) => {
                            this.trips = response.data.items;
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                }
            },
            created() {
                this.getAll();
            },
        });
    </script>
</body>

</html>