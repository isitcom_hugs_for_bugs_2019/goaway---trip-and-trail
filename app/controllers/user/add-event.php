<html>

<head>
    <title><?= APP_NAME ?></title>
    <?php Func::shared("head") ?>
</head>

<body>
    <div id="app">
        <?php Func::shared("header") ?>
        <?php include "components/nav.php" ?>
        <div class="dashboard-content">
            <div class="add-listing-headline">
                <h3><i class="sl sl-icon-doc"></i> Add an event </h3>
            </div>

            <div id="add-listing">
                <!-- Section -->
                <div class="add-listing-section">

                    <form>
                        <div class="row with-forms  padding-top-15">

                            <!-- Type -->
                            <div class="col-md-6">
                                <h5>Title</h5>
                                <input type="text" v-model="form.title" placeholder="Ex : Sticky Band">
                            </div>

                            <!-- Status -->
                            <div class="col-md-3">
                                <h5>Category</h5>
                                <select data-placeholder="All Categories" v-model="form.category_id" class="chosen-select">
                                    <option value="1">Sahara</option>
                                    <option value="2">Forest</option>
                                    <option value="3">Snow</option>
                                    <option value="4">Beach</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <h5>Price</h5>
                                <div class="fm-input pricing-price"><input type="text" v-model="form.price" placeholder="0.00" data-unit="USD" /></div>
                            </div>
                            <div class="col-md-12">
                                <h5>Description</h5>
                                <textarea type="text" v-model="form.description" placeholder=""></textarea>
                            </div>


                            <div class="col-md-3">
                                <h5>City</h5>
                                <input type="text" v-model="form.city" placeholder="paris,Lyon">
                            </div>


                            <div class="col-md-3">
                                <h5>Distance</h5>
                                <div class="fm-input"><input type="text" value="4" v-model="form.distance" placeholder="20 km" /></div>
                            </div>

                            <div class="col-md-3">
                                <h5>Duration</h5>
                                <input type="text" v-model="form.duration" value="4" placeholder="6 Days">
                            </div>
                            <div class="col-md-3">
                                <h5>Date</h5>
                                <input type="text" id="date-picker" v-model="form.date" placeholder="Date" readonly="readonly">




                            </div>
                    </form>

                    <div class="col-md-12">
                        <a class="button fullwidth margin-top-25" @click="add()">Add</a>
                    </div>
                </div>
            </div>

        </div>

        <?php include "components/footer.php" ?>
    </div>

    <?php Func::shared("script") ?>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                form: {
                    title: "Event",
                    category_id: 1,
                    price: 5000,
                    description: "Desc",
                    image: "null",
                    city: "Paris",
                    distance: 10,
                    duration: 6,
                    date: '09/02/2019'
                }
            },
            methods: {
                add() {
                    const headers = {
                        'X-Token': getCookie("X-Token")
                    }
                    axios.post("<?= Func::pathApi("user/event") ?>", this.form, {
                            headers: headers
                        })
                        .then((response) => {
                            location.href = "<?= Func::path("user/event") ?>"
                        })
                }
            }
        });


        $(function() {
            $('#date-picker').daterangepicker({
                "opens": "left",
                singleDatePicker: true,
                isInvalidDate: function(date) {
                    var disabled_start = moment('09/02/2018', 'MM/DD/YYYY');
                    var disabled_end = moment('09/06/2018', 'MM/DD/YYYY');
                    return date.isAfter(disabled_start) && date.isBefore(disabled_end);
                }
            });
        }); // Calendar animation
        $('#date-picker').on('showCalendar.daterangepicker', function(ev, picker) {
            $('.daterangepicker').addClass('calendar-animated');
        });
        $('#date-picker').on('show.daterangepicker', function(ev, picker) {
            $('.daterangepicker').addClass('calendar-visible');
            $('.daterangepicker').removeClass('calendar-hidden');
        });
        $('#date-picker').on('hide.daterangepicker', function(ev, picker) {
            $('.daterangepicker').removeClass('calendar-visible');
            $('.daterangepicker').addClass('calendar-hidden');
        });


        // $("#add-event").submit(function(e) {
        //     e.preventDefault();

        //     var form = new FormData(this);
        //     var url = $(this).attr("action");
        //     $.ajax({
        //         type: "POST",
        //         beforeSend: function(request) {
        //             request.setRequestHeader("X-Token", getCookie("X-Token"));
        //         },
        //         url: url,
        //         processData: false,
        //         contentType: false,
        //         data: form,
        //         success: function(data) {

        //         }
        //     });
        // });
    </script>

</body>

</html>