<a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>
<div class="dashboard-nav">
    <div class="dashboard-nav-inner">

        <ul data-submenu-title="Main">
            <li><a href="<?= Func::path("user/event") ?>"><i class="fa fa-calendar-check-o"></i> My trips</a></li>
            <li><a href="<?= Func::path("user/booking") ?>"><i class="sl sl-icon-wallet"></i> My bookings </a></li>
        </ul>



        <ul data-submenu-title="Account">
            <li><a><i class="sl sl-icon-user"></i> My Profile</a></li>
            <li class="logout"><a><i class="sl sl-icon-power "></i> Logout</a></li>
        </ul>

    </div>
</div>