<?php


App::all('/', function () {
    include_once "home.php";
});


App::all('/add-event', function () {
    include_once "add-event.php";
});


App::all('/update-event/{id}', function ($id) {
    include_once "update-event.php";
});

App::all('/update-profil/', function () {
    include_once "update-profil.php";
});

App::all('/event', function () {
    include_once "event.php";
});


App::all('/booking', function () {
    include_once "booking.php";
});
