<?php
App::all('/', function () {
    include_once "home.php";
});

App::all('/find', function () {
    include_once "find.php";
});

App::all('/404', function () {
    include_once "404.php";
});

App::all('/login', function () {
    include_once "login.php";
});

App::all('/event', function () {
    include_once "event.php";
});