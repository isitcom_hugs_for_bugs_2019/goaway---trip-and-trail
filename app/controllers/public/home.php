<html>

<head>
    <title><?= APP_NAME; ?></title>
    <?php Func::shared("head") ?>
</head>

<body class="transparent-header">

    <div id="app">
        <?php Func::shared("header") ?>

        <div class="main-search-container centered" data-background-image="<?= Func::assets("assets/image/main-background-01.jpg") ?>">

            <div class="main-search-inner">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>
                                Go
                                <!-- Typed words can be configured in script settings at the bottom of this HTML file -->
                                <span class="typed-words"></span>
                            </h2>
                            <h4>Expolore top-rated attractions, activities and more</h4>

                            <form action="find">
                                <div class="main-search-input">

                                    <div class="main-search-input-item">
                                        <input type="text" name="q" placeholder="What are you looking for?" value="" />
                                    </div>

                                    <div class="main-search-input-item location">
                                        <div id="autocomplete-container">
                                            <input id="autocomplete-input" name="location" type="text" placeholder="Location">
                                        </div>
                                        <a href="#"><i class="fa fa-map-marker"></i></a>
                                    </div>

                                    <div class="main-search-input-item">
                                        <select data-placeholder="All Categories" name="category" class="chosen-select">
                                            <option value="all">All Categories</option>
                                            <option value="sahara">Sahara</option>
                                            <option value="forest">Forest</option>
                                            <option value="snow">Snow</option>
                                            <option value="beach">Beach</option>
                                        </select>
                                    </div>

                                    <button class="button" onclick="window.location.href='listings-half-screen-map-list.html'">Search</button>

                                </div>
                            </form>
                        </div>
                    </div>


                </div>

            </div>
        </div>

        <?php include "components/footer.php" ?>
    </div>

    <?php Func::shared("script") ?><script>
        var typed = new Typed('.typed-words', {
            strings: ["away", "find yourself again", "discover a new world"],
            typeSpeed: 80,
            backSpeed: 80,
            backDelay: 4000,
            startDelay: 1000,
            loop: true,
            showCursor: true
        });
    </script>
</body>

</html>