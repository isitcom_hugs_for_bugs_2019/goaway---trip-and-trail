<html>

<head>
    <title><?= APP_NAME ?></title>
    <?php Func::shared("head") ?>
</head>

<body>
    <div id="app">
        <?php Func::shared("header") ?>


        <!-- Gradient-->
        <div class="single-listing-page-titlebar"></div>


        <!-- Content
================================================== -->
        <div class="container">
            <div class="row sticky-wrapper">
                <div class="col-lg-8 col-md-8">

                    <!-- Titlebar -->
                    <div id="titlebar" class="listing-titlebar">
                        <div class="listing-titlebar-title">
                            <h2>Event Title</h2>
                            <span>
                                <a href="#listing-location" class="listing-address">
                                    <i class="fa fa-map-marker"></i>
                                    Sousse
                                </a>
                            </span>
                        </div>
                    </div>

                    <!-- Listing Nav -->
                    <div id="listing-nav" class="listing-nav-container">
                        <ul class="listing-nav">
                            <li><a href="#listing-overview" class="active">Overview</a></li>
                            <li><a href="#listing-location">Gellery</a></li>
                        </ul>
                    </div>

                    <!-- Overview -->
                    <div id="listing-overview" class="listing-section">

                        <!-- Description -->
                        <p>
                            Description </p>
                        <h4 class="headline margin-top-70 margin-bottom-30">Details</h4>
                        <table class="basic-table">
                            <tr>
                                <td>Date of travel</td>
                                <td>14/08/2020</td>
                            </tr>
                            <tr>
                                <td>Duration</td>
                                <td>6 days</td>
                            </tr>

                            <tr>
                                <td>Total Distance</td>
                                <td>77km</td>
                            </tr>

                            <tr>
                                <td>Price</td>
                                <td>1,390$</td>
                            </tr>


                        </table>

                        <div class="clearfix"></div>



                    </div>




                </div>


                <!-- Sidebar
		================================================== -->
                <div class="col-lg-4 col-md-4 margin-top-75 sticky">

                    <!-- Book Now -->
                    <div id="booking-widget-anchor" class="boxed-widget booking-widget margin-top-35">
                        <h3><i class="fa fa-calendar-check-o "></i> Booking</h3>
                        <div class="row with-forms  margin-top-0">

                            <!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
                            <div class="col-lg-12">
                                <!-- <input type="text" id="date-picker" placeholder="Date" readonly="readonly"> -->
                            </div>


                        </div>

                        <!-- Book Now -->
                        <a href="pages-booking.html" class="button book-now fullwidth margin-top-5">Request To Book</a>

                        <!-- Estimated Cost -->
                        <div class="booking-estimated-cost">
                            <strong>Total Cost</strong>
                            <span>$49</span>
                        </div>
                    </div>
                    <!-- Book Now / End -->


                    <div class="boxed-widget margin-top-35">
                        <div class="hosted-by-title">
                            <h4><span>Hosted by</span> <a href="pages-user-profile.html">Amine Kammoun</a></h4>
                            <a href="pages-user-profile.html" class="hosted-by-avatar"><img src="<?= Func::assets("assets/image/avatar-icon.jpg") ?>" alt=""></a>
                        </div>
                        <ul class="listing-details-sidebar">
                            <li><i class="sl sl-icon-phone"></i> 20 710 484</li>
                            <li><i class="fa fa-envelope-o"></i> <a href="#"><span class="__cf_email__">amine@kammoun.net</span></a></li>
                        </ul>



                        <!-- Reply to review popup -->
                        <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                            <div class="small-dialog-header">
                                <h3>Send Message</h3>
                            </div>
                            <div class="message-reply margin-top-0">
                                <textarea cols="40" rows="3" placeholder="Your message to Amine"></textarea>
                                <button class="button">Send Message</button>
                            </div>
                        </div>

                        <a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim">
                            <i class="sl sl-icon-envelope-open"></i> Send Message</a>
                    </div>
                    <!-- Contact / End-->



                </div>
                <!-- Sidebar / End -->

            </div>
        </div>


        <?php include "components/footer.php" ?>
    </div>

    <?php Func::shared("script") ?>

</body>

</html>