<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="<?= Func::assets('assets/css/style.css') ?>">
<link rel="stylesheet" href="<?= Func::assets('assets/css/main-color.css') ?>">
<link rel="stylesheet" href="<?= Func::assets('assets/css/main.css') ?>">
