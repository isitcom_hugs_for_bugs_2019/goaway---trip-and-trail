<header id="header-container">

    <!-- Header -->
    <div id="header">
        <div class="  margin-left-5 margin-right-10">

            <!-- Left Side Content -->
            <div class="left-side">

                <!-- Logo -->
                <div id="logo">
                    <a href="<?= Func::path("") ?>"><?= APP_NAME; ?></a>
                </div>

                <!-- Mobile Navigation -->
                <div class=" mmenu-trigger">
                    <button class="hamburger hamburger--collapse" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>

                <!-- Main Navigation -->
                <nav id="navigation" class="style-1">
                    <ul id="responsive">


                        <li><a href="#">Tours</a>

                        </li>
                        <li><a href="#">Hiking Guide</a>

                        </li>
                        <li><a href="#">About us</a>

                        </li>
                    </ul>
                </nav>
                <div class="clearfix"></div>
                <!-- Main Navigation / End -->

            </div>
            <!-- Left Side Content / End -->


            <!-- Right Side Content / End -->
            <div class="right-side">
                <div class="header-widget">
                    <!-- User Menu -->
                    <?php if (IS_LOGIN == "OK") { ?>

                        <div class="user-menu">
                            <div class="user-name"><span><img src="<?= Func::assets("assets/image/avatar-icon.jpg") ?>" alt="">
                                </span>Hi,
                                <?= TOKEN_FIRST_NAME ?>
                            </div>
                            <ul>
                                <li><a href="<?= Func::path("user/event") ?>"><i class="fa fa-calendar-check-o"></i>
                                        My events</a></li>
                                <li><a href="<?= Func::path("user/booking") ?>"><i class="fa fa-calendar-check-o"></i>
                                        My bookings</a></li>
                                <li class="logout"><a><i class="sl sl-icon-power"></i> Logout</a></li>
                            </ul>
                        </div>
                        <a href="<?= Func::path("user/add-event") ?>" class="button border with-icon">Add Event <i class="sl sl-icon-plus"></i></a>
                    <?php } else { ?>
                        <a href="#sign-in-dialog" class="button border with-icon popup-with-zoom-anim"><i class="sl sl-icon-login  margin-right-10 "></i> Log in</a>
                    <?php } ?> </div>
            </div>
            <!-- Right Side
                        Content / End -->

            <!-- Sign In Popup -->
            <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

                <div class="small-dialog-header">
                    <h3>Sign In</h3>
                </div>

                <!--Tabs -->
                <div class="sign-in-form style-1">

                    <ul class="tabs-nav">
                        <li class=""><a href="#tab1">Log In</a></li>
                        <li><a href="#tab2">Register</a></li>
                    </ul>

                    <div class="tabs-container alt">

                        <!-- Login -->
                        <div class="tab-content" id="tab1" style="display: none;">
                            <form method="post" action="<?= Func::pathApi("auth/signin") ?>" id="register" class="register">

                                <p class="form-row form-row-wide">
                                    <label for="email">Email:
                                        <i class="im im-icon-Male"></i>
                                        <input type="text" class="input-text" name="email" id="email" value="" />
                                    </label>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="password">Password:
                                        <i class="im im-icon-Lock-2"></i>
                                        <input class="input-text" placeholder="password" type="password" name="password" id="password" />
                                    </label>
                                </p>

                                <div class="form-row">
                                    <input type="submit" class="button border margin-top-5" name="login" value="Login" />
                                </div>

                            </form>
                        </div>

                        <!-- Register -->
                        <div class="tab-content" id="tab2" style="display: none;">

                            <form method="post" class="register" action="<?= Func::pathApi("auth/signup") ?>" id="register">

                                <p class="form-row form-row-wide">
                                    <label for="username2">First name:
                                        <i class="im im-icon-Male"></i>
                                        <input type="text" class="input-text" name="first_name" value="" />
                                    </label>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="email">Last name:
                                        <i class="im im-icon-Male"></i>
                                        <input type="text" class="input-text" name="last_name" id="email" value="" />
                                    </label>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="email">Email:
                                        <i class="im im-icon-email"></i>
                                        <input type="text" class="input-text" name="email" id="email" value="" />
                                    </label>
                                </p>

                                <p class="form-row form-row-wide">
                                    <label for="password">Password:
                                        <i class="im im-icon-Lock-2"></i>
                                        <input class="input-text" placeholder="password" type="password" name="password" id="password" />
                                    </label>
                                </p>


                                <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />

                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Sign In Popup / End -->

        </div>
    </div>
    <!-- Header / End -->

</header>
<div class="clearfix"></div>