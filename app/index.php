<?php require_once 'core.php';
header("Access-Control-Max-Age: 3600");

//REQUIRES
require_once './classes/Func.php'; //Shared function (STATIC)
require_once './classes/App.php';


//classes initialisation
App::init();


/**** ROUTES *****/


$routes = json_decode(file_get_contents('./routes.json'), true);
// Before Router Middleware 
foreach ($routes["auth"] as $key => $item) {

    (!array_key_exists("methods", $item)) ?
        $_methods = 'GET|POST|PUT|DELETE|PATCH' : $_methods =  preg_replace('/\s+/', '', strtoupper($item["methods"]));

    App::before(
        $_methods,
        preg_replace('/\s+/', '', $item["paths"]),
        function ()  use ($item) {
            $token = Func::getCookie(ID_TOKEN_KEY);
            $curl_h = curl_init(API_URL . 'auth/validation');
            curl_setopt(
                $curl_h,
                CURLOPT_HTTPHEADER,
                array(
                    ID_TOKEN_KEY . ": " . $token
                )
            );
            curl_setopt($curl_h, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl_h);
            $response = json_decode($response);

            if (!defined('IS_LOGIN')) {
                if ($response->status == "OK") {
                    foreach ($response->info as $key => $token_key) {
                        define(strtoupper("TOKEN_" . $key), $token_key);
                    }
                }
                define("IS_LOGIN", $response->status);
            }
        }
    );
}

// Custom mount Handler -- (Controllers)
foreach ($routes["mounts"] as $key => $item) {
    if ($item["path"] == "/") {
        include_once('./controllers/' . $item["controller"]);
    } else {
        App::mount($item["path"], function () use ($item) {
            include_once('./controllers/' . $item["controller"]);
        });
    }
}
/**** ROUTES *****/

// Custom 404 Handler
App::set404(function () {
    http_response_code(404);
    include_once("./controllers/public/404.php");
});

//Thunderbirds are go!
App::run(function () {
    http_response_code(200);
});
